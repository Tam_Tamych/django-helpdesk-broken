from os import environ, system, listdir, remove, path
from shutil import rmtree, move, copy2
from time import time
from enum import Enum
import logging
import shutil
import zipfile
import argparse

logging.basicConfig(
    level=logging.DEBUG,
    format="[%(asctime)s] %(levelname)-12s|process:%(process)-5s|thread:%(thread)-5s|funcName:%(funcName)s|message:%(message)s",
    handlers=[
        # logging.FileHandler('fileName.log'),
        logging.StreamHandler()
    ])


def log_wrapper(wrapped_func):
    def wrapper(*args):
        logging.info(f'STARTING {wrapped_func.__name__} ...')
        start_func_time = round(time() * 1000)
        wrapped_func(*args)
        end_func_time = round(time() * 1000)
        work_time_milli = end_func_time - start_func_time
        logging.info(
            f'ENDED {wrapped_func.__name__} in {work_time_milli:_} millis')
    return wrapper


def run_shell_command(command: str):
    complete_code = system(command)
    if complete_code != 0:
        raise SystemExit()


class Settings(Enum):
    REPO_FOLDER_NAME = './'
    HELPDESK_FILES_FOLDER_NAME = 'helpdesk_files'


@log_wrapper
def copytree(src, dst, symlinks=False, ignore=None):
    for item in listdir(src):
        s = path.join(src, item)
        d = path.join(dst, item)
        if path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)


@log_wrapper
def set_envs():
    for setings_key in Settings:
        logging.debug(
            f'setings_key.name <type:{type(setings_key.name)}> = {setings_key.name}')
        logging.debug(
            f'setings_key.value <type:{type(setings_key.value)}> = {setings_key.value}')
        logging.info(f'Set env {setings_key.name}={setings_key.value}')
        environ[setings_key.name] = str(setings_key.value)
        logging.info(
            f'Check env {setings_key.name}={environ.get(setings_key.name)}')



@log_wrapper
def transfer_docker_files():
    copy2(
        path.join(
            Settings.HELPDESK_FILES_FOLDER_NAME.value,
            'entrypoint.sh',
        ),
        path.join(
            f'{Settings.REPO_FOLDER_NAME.value}',
            'entrypoint.sh',
        ),
    )


@log_wrapper
def transfer_settings_files():
    settings_file_path = path.join(
        Settings.HELPDESK_FILES_FOLDER_NAME.value,
        'settings.py',
    )
    with open(settings_file_path, encoding='UTF-8') as settings_file:
        logging.info('Print settings.py...')
        for line in settings_file.readlines():
            logging.info(line)
        logging.info('Print settings.py complete')
    copy2(
        settings_file_path,
        path.join(
            f'{Settings.REPO_FOLDER_NAME.value}',
            'demo',
            'demodesk',
            'config',
            'settings.py',
        ),
    )
@log_wrapper
def transfer_fixture_file():
    fixture_file_path = path.join(
        Settings.HELPDESK_FILES_FOLDER_NAME.value,
        'fixture.json',
    )
    with open(fixture_file_path, encoding='UTF-8') as fixture_file:
        logging.info('Print settings.py...')
        for line in fixture_file.readlines():
            logging.info(line)
        logging.info('Print fixture.json complete')
    copy2(
        fixture_file_path,
        path.join(
            f'{Settings.REPO_FOLDER_NAME.value}',
            'demo',
            'demodesk',
            'fixtures',
            'fixture.json',
        ),
    )


@log_wrapper
def create_helpdesk_django_project():
    transfer_docker_files()
    transfer_settings_files()
    transfer_fixture_file()


def build_and_run_docker():
    run_shell_command('sudo docker-compose config')
    run_shell_command('sudo docker-compose build --no-cache')
    run_shell_command('sudo docker-compose up -d')


@log_wrapper
def main():
    parser = argparse.ArgumentParser(description='Sandbox creator utility')
    parser.add_argument('--deploy', dest='deploy',
                        default=False,
                        help='This args for GitLab CICD')
    args = parser.parse_args()

    set_envs()
    create_helpdesk_django_project()
    if args.deploy is False:
        build_and_run_docker()


if __name__ == "__main__":
    main()
